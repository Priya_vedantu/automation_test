//Create one more class like AutomationEngineer which extends Employee(have attributes like Skills, ExpertInSkills, Project associated to etc…)
//Declare proper methods read above values(getters and setters)

import { Employee } from "./Employee1";

class AutomationEngineer extends Employee{
    Skills: string;
    ExpertInSkills:string;
    constructor(Skills:string, ExpertInSkills:string, FName: string,  Salary:number ) {
        super(FName, Salary);
        this.Skills = Skills;
        this.ExpertInSkills=ExpertInSkills;
        }
        getSkills(): string{
            return this.Skills;
        }
        getExpertInSkills():string{
            return this.ExpertInSkills;
        }
}

var ob =  new AutomationEngineer("Testing", "Functional Testing", "Priya", 10000);
console.log(ob.getFName(), ob.getSalary(),ob.getSkills(), ob.getExpertInSkills());

var ob1 = new Employee("Priya", 10000)
console.log(ob1.getFName(),ob1.getSalary()); 
