const arr = [34, 67, 31, 87, 12, 30, 22];
const findThirdMax = (arr = []) => {
   const map = {};
   let j = 0;
   for (let i = 0, l = arr.length; i < l; i++) {
      if(!map[arr[i]]){
         map[arr[i]] = true;
      }else{
         continue;
      };
      arr[j++] = arr[i];
   };
   arr.length = j;
   let result = -Infinity;
   if (j < 3) {
      for (let i = 0; i < j; ++i) {
         result = Math.max(result, arr[i]);
      }
      return result;
   } else {
      arr.sort(function (prev, next) {
         if (next >= prev) return -1;
         return 1;
      });
      return arr[j - 3]
   };
};
console.log(findThirdMax(arr));
