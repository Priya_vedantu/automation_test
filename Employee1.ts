//Create class Employee(have fields like First Name, Last name, age, salary etc...) use constructors and getters and setters
//Declare methods like get name, get salary etc
export class Employee{
    FName:string;
    Salary:number;
    constructor(FName:string, Salary:number){
        this.FName=FName;
        this.Salary=Salary;
    }
    getFName():string{
        return this.FName;
    }
    getSalary():number{
        return this.Salary;
    }
}
var ob = new Employee("Priya", 100);
console.log(ob.getFName(), ob.getSalary());





/*class Employee{
    fName= "Priya";
    salary = "1";
    constructor(fName:String, salary:String){
        this.fName=fName;
        this.salary=salary;
    }
    getFName():String{
        return this.fName;
    }
    getsalary():String{
        return this.salary;
    }
}
var ob = new Employee();
console.log(ob.getfName(),ob.getsalary()); */

