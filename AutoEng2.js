"use strict";
//Create one more class like AutomationEngineer which extends Employee(have attributes like Skills, ExpertInSkills, Project associated to etc…)
//Declare proper methods read above values(getters and setters)
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Employee1_1 = require("./Employee1");
var AutomationEngineer = /** @class */ (function (_super) {
    __extends(AutomationEngineer, _super);
    function AutomationEngineer(Skills, ExpertInSkills, FName, Salary) {
        var _this = _super.call(this, FName, Salary) || this;
        _this.Skills = Skills;
        _this.ExpertInSkills = ExpertInSkills;
        return _this;
    }
    AutomationEngineer.prototype.getSkills = function () {
        return this.Skills;
    };
    AutomationEngineer.prototype.getExpertInSkills = function () {
        return this.ExpertInSkills;
    };
    return AutomationEngineer;
}(Employee1_1.Employee));
var ob = new AutomationEngineer("Testing", "Functional Testing", "Priya", 10000);
console.log(ob.getFName(), ob.getSalary(), ob.getSkills(), ob.getExpertInSkills());
var ob1 = new Employee1_1.Employee("Priya", 10000);
console.log(ob1.getFName(), ob1.getSalary());
