//Create one more class like AutomationEngineer which extends Employee(have attributes like Skills, ExpertInSkills, Project associated to etc…)
//Declare proper methods read above values(getters and setters)
class Employee{
    constructor(First, Last){

        this.First = First;
        this.Last = Last;
    }
   getFirstname(){
       return this.First;
   }
   getLastname(){
       return this.Last;
    
   }
}
class AutomationEngineer extends Employee{
    constructor (First, Last, skills, Expertinskills){
        super(First,Last)
        this.skills = skills;
        this.Expertinskills = Expertinskills;
    
    }
     getSkills(){
         return this.skills;

     }
     getExpertinSkills(){
         return this.Expertinskills;
    }
}

const ob1 = new AutomationEngineer("abc", "xyz", "testing", "Functional_Testing");
console.log(ob1.getSkills());
console.log(ob1.getExpertinSkills());

class QAEngineer extends Employee{
    constructor (First, Last, skills, Expertinskills){
        super(First,Last)
        this.skills = skills;
        this.Expertinskills = Expertinskills;
    
    }
     getSkills(){
         return this.skills;

     }
     getExpertinSkills(){
         return this.Expertinskills;
    }
}

class Maneger extends Employee{
    constructor (First, Last, skills, Expertinskills){
        super(First,Last)
        this.skills = skills;
        this.Expertinskills = Expertinskills;
    
    }
     getSkills(){
         return this.skills;

     }
     getExpertinSkills(){
         return this.Expertinskills;
    }
}

const obM = new Maneger("Mabe", "Mxyz", "Mskills","Eskills");
const obQA = new QAEngineer("Qaabc","Qaxyz","Qaskills", "QaEskills");
console.log(obM.getSkills());
console.log(obQA.getExpertinSkills());


