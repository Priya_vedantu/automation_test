//Create class Employee(have fields like First Name, Last name, age, salary etc...) use constructors and getters and setters
//Declare methods like get name, get salary etc…
class Employee{
    constructor(FirstName, LastName, Age, Salary){
        this.FirstName= FirstName;
        this.LastName = LastName;
        this.Age = Age;
        this.Salary= Salary;
}
getFirstName(){
    return this.FirstName;
}
getLastName(){
    return this.LastName;
}
getAge(){
    return this.Age;
}
getSalary(){
    return this.Salary;
}
}
const ob = new Employee("sita", "Ram", 25, 10000);
console.log(ob.getFirstName());
console.log(ob.getLastName());
console.log(ob.getAge());
console.log(ob.getSalary());