"use strict";
//Create one more class like QAEngineer which extends Employee(have attributes like Skills, ExpertInSkills, Project associated to etc…)
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Employee1_1 = require("./Employee1");
//Declare proper methods read above values(getters and setters)
var QaEngineer = /** @class */ (function (_super) {
    __extends(QaEngineer, _super);
    function QaEngineer(Skills, ExpertInSkills, FName, Salary, ProjectName) {
        var _this = _super.call(this, FName, Salary) || this;
        _this.Skills = Skills;
        _this.ExpertInSkills = ExpertInSkills;
        _this.ProjectName = ProjectName;
        return _this;
    }
    QaEngineer.prototype.getSkills = function () {
        return this.Skills;
    };
    QaEngineer.prototype.getExpertInSkills = function () {
        return this.ExpertInSkills;
    };
    QaEngineer.prototype.getProjectName = function () {
        return this.ProjectName;
    };
    return QaEngineer;
}(Employee1_1.Employee));
var ob = new QaEngineer("Testing", "Functional Testing", "Priya", 10000, "MasterTalk");
console.log(ob.getFName(), ob.getSalary(), ob.getSkills(), ob.getExpertInSkills(), ob.getProjectName());
var ob1 = new Employee1_1.Employee("Priya", 10000);
console.log(ob1.getFName(), ob1.getSalary());
