//Create one more class like QAEngineer which extends Employee(have attributes like Skills, ExpertInSkills, Project associated to etc…)

import { Employee } from "./Employee1";

//Declare proper methods read above values(getters and setters)
class QaEngineer extends Employee{
    Skills: string;
    ExpertInSkills:string;
    ProjectName: string;
    constructor(Skills:string, ExpertInSkills:string, FName: string,  Salary:number, ProjectName:string ) {
        super(FName, Salary);
        this.Skills = Skills;
        this.ExpertInSkills=ExpertInSkills;
        this.ProjectName = ProjectName;
        }
        getSkills(): String{
            return this.Skills;
        }
        getExpertInSkills():string{
            return this.ExpertInSkills;
        }
        getProjectName(): string{
            return this.ProjectName;
        }
}

var ob =  new QaEngineer("Testing", "Functional Testing", "Priya", 10000, "MasterTalk");
console.log(ob.getFName(), ob.getSalary(),ob.getSkills(), ob.getExpertInSkills(), ob.getProjectName());

var ob1 = new Employee("Priya", 10000)
console.log(ob1.getFName(),ob1.getSalary());
