//Print HELLO in CAPITAL letters for the given string "ahceclwlxo"
let str = "ahceclwlxo";
let starArray = str.split("");
let resultString = "HELLO";
let output = "";
starArray.map((string) => {
  if (string === "h") {
    output += "H";
  } else if (string === "e") {
    output += "E";
  } else if (string === "l") {
    output += "L";
  } else if (string === "o") {
    output += "O";
  } else if (string === "e") {
    output += "E";
  } else {
    output += string;
  }
  return null;
});
console.log("output-----", output);