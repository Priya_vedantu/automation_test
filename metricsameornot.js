const N = 4;

function metricsame(A, B)
{
    let i, j;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++)
            if (A[i][j] != B[i][j])
                return 0;
    return 1;
}
let A = [ [1, 1, 1, 1],
              [2, 2, 2, 2],
              [3, 3, 3, 3],
              [4, 4, 4, 4]];
 
    let B = [ [1, 1, 1, 1],
              [2, 2, 2, 2],
              [3, 3, 3, 3],
              [4, 4, 4, 4]];
 
    if (metricsame(A, B))
        document.write("Matrices are identical");
    else
        document.write("Matrices are not identical");
