/*Create class Employee(have fields like First Name, Last name, age, salary etc...)
 use constructors and getters and setters*/
   class Employee {
           EmpFname : string;
           EmpLname : string;
           EmpAge   : number;
           EmpSalary: number;

           constructor (EmpFname: string,EmpLname: string, EmpAge: number, EmpSalary: number )
           {
                   this.EmpFname = EmpFname;
                   this.EmpLname = EmpLname;
                   this.EmpAge = EmpAge;
                   this.EmpSalary= EmpSalary;
           }
   } 
    
   let emp = new Employee("Ram", "abc", 25, 20000);
