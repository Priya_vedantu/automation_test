"use strict";
exports.__esModule = true;
exports.Employee = void 0;
//Create class Employee(have fields like First Name, Last name, age, salary etc...) use constructors and getters and setters
//Declare methods like get name, get salary etc
var Employee = /** @class */ (function () {
    function Employee(FName, Salary) {
        this.FName = FName;
        this.Salary = Salary;
    }
    Employee.prototype.getFName = function () {
        return this.FName;
    };
    Employee.prototype.getSalary = function () {
        return this.Salary;
    };
    return Employee;
}());
exports.Employee = Employee;
var ob = new Employee("Priya", 100);
console.log(ob.getFName(), ob.getSalary());
/*class Employee{
    fName= "Priya";
    salary = "1";
    constructor(fName:String, salary:String){
        this.fName=fName;
        this.salary=salary;
    }
    getFName():String{
        return this.fName;
    }
    getsalary():String{
        return this.salary;
    }
}
var ob = new Employee();
console.log(ob.getfName(),ob.getsalary()); */
